package ru.tsc.tsepkov.tm.exception.user;

public final class RoleEmptyException extends AbstractUserException {

    public RoleEmptyException() {
        super("Error! Role is empty...");
    }

}
